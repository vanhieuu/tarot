import React from 'react'
import { StyleSheet,  } from 'react-native'
import { GestureHandlerRootView } from 'react-native-gesture-handler'
import RootStack from './src/nav/RootStack'

const App = () => {
  return (
    <GestureHandlerRootView style={{flex:1}}>
    <RootStack/>
    </GestureHandlerRootView>
  )
}

export default App

const styles = StyleSheet.create({})
