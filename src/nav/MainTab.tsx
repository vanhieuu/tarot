/* eslint-disable prettier/prettier */
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DrawCard from '../screen/MainTab/DrawCard';

import Discover from '../screen/Discover';
export type MainTabParamList = {
  DrawCard: undefined;
  Discover: undefined;
};
const Tab = createBottomTabNavigator();

const MainTab = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName = '';
          if (route.name === 'DrawCard') {
            iconName = focused ? 'albums-outline' : 'albums';
          } else if (route.name === 'Discover') {
            iconName = focused ? 'ios-planet-outline' : 'md-planet-sharp';
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        //console.log
        tabBarActiveTintColor: '#7e42f5',
        tabBarInactiveTintColor: 'gray',
      })}>
      <Tab.Screen
        name="DrawCard"
        component={DrawCard}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Discover"
        component={Discover}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

export default MainTab;
