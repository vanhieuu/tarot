/* eslint-disable prettier/prettier */
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import Discover from '../screen/Discover';
import MainScreen from '../screen/MainScreen';
import DrawCard from '../screen/MainTab/DrawCard';
import Reading from '../screen/MainTab/Reading';
import Today from '../screen/MainTab/Today';
import {ITarot} from '../types/ITarot';
import MainTab from './MainTab';

export type RootStackParamList = {
  MainTab: undefined;
  Discover: undefined;
  MainScreen: undefined;
  Reading: {
    id: string;
    img: string;
    meaning_up: string;
    desc: string;
  };

  Today: {
    listCard: ITarot[];
  };
  DrawCard: undefined;
};
const Stack = createNativeStackNavigator<RootStackParamList>();

const RootStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="MainScreen">
        <Stack.Screen
          name="MainScreen"
          component={MainScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DrawCard"
          component={DrawCard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MainTab"
          component={MainTab}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Discover"
          component={Discover}
          options={{headerShown: true}}
        />
        <Stack.Screen name="Reading" component={Reading} />

        <Stack.Screen
          name="Today"
          component={Today}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootStack;
