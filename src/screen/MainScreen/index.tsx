import { NavigationProp, useNavigation } from "@react-navigation/core";
import React from "react";
import {
  Button,
  Dimensions,
  ImageBackground,
  StyleSheet,
  Text,
  View,
} from "react-native";
import Sound from "react-native-sound";
import { RootStackParamList } from "../../nav/RootStack";

const widthScreen = Dimensions.get("window").width;
const height = (widthScreen * 415) / 291;
const MainScreen = () => {
  const { navigate } = useNavigation<NavigationProp<RootStackParamList>>();
  var onClickSound = new Sound('onclick.mp3')
  onClickSound.setVolume(1.0)
  return (
    <View
      style={{
        flex: 1,
      }}
    >
      <ImageBackground
        source={require("../../assets/bgTop.jpg")}
        resizeMode="cover"
        style={{
          width: "100%",
          height: "100%",
        }}
        blurRadius={10}
      >
        <Text
          style={styles.txtWelcome}
        >
          Welcome {`\n`} To {`\n`} MyTarot
        </Text>
        <View
          style={{
            marginHorizontal: 50,
            marginTop:50,
            marginBottom: 10,
            borderRadius: 10,
            height:50,
          }}
        >
          <Button
            title="Draw 1 Card"
            onPress={() => {
              onClickSound.play()
              navigate("MainTab");
            }}
          />

        </View>
        <View
          style={{
            flex: 1,
            height:50,
            marginHorizontal: 50,
          }}
        >
          <Button
            title="Discover All Card"
            onPress={() => {
              onClickSound.play()
              navigate("Discover");
            }}
          />

        </View>
      </ImageBackground>
    </View>
  );
};

export default MainScreen;

const styles = StyleSheet.create({
  txtWelcome:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    fontSize: 60,
    color: "#F8D8AE",
    fontWeight: "bold",
    marginTop:100
  }

});
