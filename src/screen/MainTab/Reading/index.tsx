import {
  NavigationProp,
  RouteProp,
  useNavigation,
  useRoute,
} from '@react-navigation/core';
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  ImageBackground,
  Dimensions,
  Button,
} from 'react-native';
import Sound from 'react-native-sound';

import {RootStackParamList} from '../../../nav/RootStack';

const widthScreen = Dimensions.get('window').width;
const height = (widthScreen * 1060) / 1000;

const Reading = () => {
  const route = useRoute<RouteProp<RootStackParamList, 'Reading'>>();
  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();
  const cards = route.params;
  var onClickSound = new Sound('onclick.mp3')
  onClickSound.setVolume(1.0)
  const goMainScreen = React.useCallback(() => {
    onClickSound.play()
    navigate('MainScreen');
  }, []);

  return (
    <ImageBackground
      source={{uri: cards.img}}
      style={{width: '100%', height: '100%'}}
      resizeMode="cover"
      blurRadius={50}>
      <ScrollView
        style={{
          flex: 1,
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={{uri: cards.img}}
            style={{width: 100, height: 200}}
            resizeMode="contain"
          />
          <Text style={styles.txtMeaning}>{cards.meaning_up}</Text>
          <Text style={styles.txtDesc}>{cards.desc}</Text>
        </View>
        <View>
          <Button onPress={goMainScreen} title="Go to MainScreen" />
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

export default Reading;
const styles = StyleSheet.create({
  txtMeaning: {
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    textShadowOffset: {
      width: 1,
      height: 1,
    },
    textShadowColor: 'black',
    textShadowRadius: 1,
    marginBottom: 20,
    marginTop: 10,
  },
  txtDesc: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 20,
  },
});
