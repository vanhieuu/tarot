import React from "react";
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Animated,
  Image,
} from "react-native";
import { ITarot } from "../../../../types/ITarot";

const widthScreen = Dimensions.get("window").width;
const heightCard = (widthScreen * 1060) / 1000;
const imageW = widthScreen * 0.7;
const imageH = imageW * 1.54;

interface Props extends ITarot {
  onPress: () => void; 
}

const ItemRender = ({ backCard,  onPress,  }: Props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        <Animated.Image
          source={{ uri: backCard }}
          style={[styles.img]}
          resizeMode="cover"
        />
      </TouchableOpacity>
    </View>
  );
};

export default ItemRender;

const styles = StyleSheet.create({
  container: {
    width: widthScreen,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 20,
  },
  img: {
    width: imageW,
    height: imageH,
    borderRadius: 16,
  },
});
