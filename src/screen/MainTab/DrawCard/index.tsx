import {NavigationProp, useNavigation} from '@react-navigation/core';
import React from 'react';
import {Dimensions, StyleSheet, View, Animated} from 'react-native';
import URL from '../../../config/Api';
import {RootStackParamList} from '../../../nav/RootStack';
import {IResTarot, ITarot} from '../../../types/ITarot';
import ItemRender from './components/ItemRender';
import Sound from 'react-native-sound';
interface Props extends ITarot {
  item: ITarot;
  index: number;
}

const widthScreen = Dimensions.get('window').width;
const heightCard = (widthScreen * 1060) / 1000;
const imageW = widthScreen * 0.7;
const imageH = imageW * 1.54;
const DrawCard = ({item, index}: Props) => {
  Sound.setCategory('Playback');
  var ding = new Sound('onclick.mp3', Sound.MAIN_BUNDLE, error => {});
  ding.setVolume(1);
  const [data, setData] = React.useState<ITarot[]>([]);
  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();
  // const route = useRoute<RouteProp<RootStackParamList>>();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [isRefresh, setIsRefresh] = React.useState(false);
  var shuffle = new Sound('shuffle.mp3');
  var slideCardSound = new Sound('scroll.mp3')
  const scrollX = React.useRef(new Animated.Value(0)).current;

  React.useEffect(() => {
    fetch(URL.Cards)
      .then(response => response.json())
      .then(cards => {
        setData(cards);
        setLoading(false);
        // console.log('cards',cards)
      })
      .catch(error => {
        console.error(error);
      });
  }, []);
  React.useEffect(() => {
    shuffle.play();
  }, []);
  React.useEffect(() => {
    slideCardSound.play()
  },[])
  const goToday = React.useCallback(() => {
    ding.play();
    navigate('Today', {
      listCard: data,
    });
  }, [data]);
  // console.log('cards',data)
  // console.log('data', data.id);
  const onRefresh = React.useCallback(() => {
    setIsRefresh(true);
    fetch(URL.Cards)
      .then(response => response.json())
      .then(cards => {
        setData(cards);
        setIsRefresh(false);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);
  const onEndReached = React.useCallback(() => {
    setData((prev: ITarot[]) => prev.concat(data));
  }, []);
  const renderItem = React.useCallback(
    ({item}: {item: ITarot}) => {
      return (
        <ItemRender
          backCard={item.backCard}
          onPress={goToday}
          id={''}
          name={''}
          value={''}
          img={''}
          meaning_up={''}
          meaning_rev={''}
          desc={''}
        />
      );
    },
    [goToday],
  );
  return (
    <View style={StyleSheet.absoluteFillObject}>
      {data.map((item, index) => {
        const inputRange = [
          (index - 1) * imageW,
          index * imageW,
          (index + 1) * imageW,
        ];
        const scale = scrollX.interpolate({
          
          inputRange,
          outputRange: [0.75, 1, 0.75],
          
        });
        const opacity = scrollX.interpolate({
          inputRange,
          outputRange: [0.8, 1, 0.8],
        });
        return (
          <Animated.Image
            key={`img-${index}`}
            source={{uri: item.backCard}}
            style={[
              StyleSheet.absoluteFillObject,
              {
                opacity,
                transform: [
                  {
                    scale,
                    
                  },
                ],
              },
            ]}
            blurRadius={50}
            resizeMode="cover"
            
          />
        );
      })}

      <Animated.FlatList
        data={data}
        renderItem={renderItem}
        horizontal
        style={styles.flatList}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{paddingHorizontal: 5}}
        refreshing={isRefresh}
        onRefresh={onRefresh}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.5}
        onEndReached={onEndReached}
        onMomentumScrollEnd={ev => {
          const index = Math.round(
            ev.nativeEvent.contentOffset.x / widthScreen,
          );
        }}
        decelerationRate="fast"
        onScroll={
          Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {useNativeDriver: true},
          
        )}
      />
    </View>
  );
};

export default DrawCard;

const styles = StyleSheet.create({
  flatList: {
    width: widthScreen,
    flex: 1,
    shadowColor: '#000',
    marginTop: 20,
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 20,
  },
  img: {
    width: '100%',
    height: '100%',
  },
});
