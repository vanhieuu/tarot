import {
  NavigationProp,
  RouteProp,
  useNavigation,
  useRoute,
} from '@react-navigation/core';
import React from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Animated,
  ImageBackground,
  Button,
} from 'react-native';
import Sound from 'react-native-sound';

import {RootStackParamList} from '../../../nav/RootStack';
import {ITarot} from '../../../types/ITarot';

const widthScreen = Dimensions.get('window').width;
const heightCard = (widthScreen * 1060) / 1000;

const Today = () => {
  const transY = React.useRef(new Animated.Value(widthScreen)).current;
  const [refreshing, setRefreshing] = React.useState<boolean>(false);
  const [cards, setCards] = React.useState<ITarot[]>([]);
  const route = useRoute<RouteProp<RootStackParamList, 'Today'>>();
  const CARDS = route.params.listCard;
  const randomCard = CARDS[Math.floor(Math.random() * CARDS.length)];
  const rotateY = transY.interpolate({
    inputRange: [0, 180],
    outputRange: ['0deg', '180deg'],
  });
  Sound.setCategory('Playback');
  var flip = new Sound('flipcard.mp3', Sound.MAIN_BUNDLE);
  flip.setVolume(1.0);
  var onClickSound = new Sound('onclick.mp3');
  onClickSound.setVolume(1.0);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
  }, []);
  const onPress = React.useCallback(() => {
    onClickSound.play();
    navigate('Reading', {
      id: randomCard.id,
      img: randomCard.img,
      meaning_up: randomCard.meaning_up,
      desc: randomCard.desc,
    });
  }, [randomCard, onRefresh]);

  const goDraw = React.useCallback(() => {
    onClickSound.play();
    navigate('DrawCard');
  }, [onRefresh]);


  React.useEffect(()=>{
    flip.play();
  },[])

  React.useEffect(() => {
    Animated.timing(transY,{
      toValue: 0,
      duration: 2000,
      useNativeDriver: true,
    }).start();
  });

  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();

  return (
    <ImageBackground
      source={{uri: randomCard.img}}
      resizeMode="cover"
      style={{
        width: '100%',
        height: '100%',
      }}
      blurRadius={50}>
      <ScrollView
        style={{
          flex: 1,
          marginTop: 20,
          marginBottom: 10,
        }}>
        <TouchableOpacity>
          <Animated.View
            style={[
              {
                backfaceVisibility: 'hidden',
                transform: [{rotateY: '180deg'}, {rotateY}],
                marginTop: 30,
                marginBottom: 30,
              },
            ]}>
            <Image
              source={{uri: randomCard.backCard}}
              resizeMode="contain"
              style={styles.img}
            />
          </Animated.View>

          <Animated.View
            style={{
              ...StyleSheet.absoluteFillObject,
              backfaceVisibility: 'hidden',
              transform: [{rotateY}],
              marginTop: 30,
              marginBottom: 30,
            }}>
            <Image
              source={{uri: randomCard.img}}
              resizeMode="contain"
              style={styles.img}
            />
          </Animated.View>
        </TouchableOpacity>
        <Text style={styles.txtCardName}>{randomCard.name}</Text>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 50,
          marginVertical: 20,
        }}>
        <Button onPress={onPress} title="Go to reading" />
      </View>
      <View
        style={{
          marginHorizontal: 50,
          marginVertical: 20,
        }}>
        <Button onPress={goDraw} title="Go to draw" />
      </View>
    </ImageBackground>
  );
};

export default Today;

const styles = StyleSheet.create({
  img: {
    width: widthScreen,
    height: heightCard,
    paddingBottom: 23,
  },
  txtCardName: {
    fontSize: 45,
    textAlign: 'center',
    fontFamily: 'Helvetica',
    marginTop: 10,
    fontWeight: 'bold',
    textShadowOffset: {
      width: 2,
      height: 2,
    },
    textShadowColor: 'white',
    textShadowRadius: 1,
  },
});
