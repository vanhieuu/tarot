/* eslint-disable prettier/prettier */
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Animated,
} from 'react-native';
import {ITarot} from '../../../types/ITarot';

interface Props extends ITarot {
  
}

const widthScreen = Dimensions.get('window').width;

const widthItem = (widthScreen - 16) / 2;

const ItemRender = ({img, name, id,}: Props) => {
  return (
    <TouchableOpacity
       style={{ justifyContent:'center',flex:1,alignItems:'center'}}
        
    >
    <View style={styles.container}>
      <Image source={{uri: img}} style={styles.img} borderRadius={6} />
      <Text style={styles.text}>{`${name}-${id}`}</Text>
    </View>
    </TouchableOpacity>
  );
};

export default ItemRender;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 30,
    width: widthItem,
    paddingHorizontal: 8,
    marginBottom: 16,
  },
  text: {
    alignSelf: 'center',
    marginTop: 12,
    marginBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  img: {
    width: 100,
    height: 200,
    paddingBottom: 23,
    marginHorizontal: 30,
    marginVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
