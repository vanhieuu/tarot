import {NavigationProp, useNavigation} from '@react-navigation/core';
import React from 'react';
import {Dimensions, FlatList, StyleSheet, View} from 'react-native';
import URL from '../../config/Api';
import {RootStackParamList} from '../../nav/RootStack';
import {ITarot} from '../../types/ITarot';
import ItemRender from './components/ItemRender';

const widthScreen = Dimensions.get('window').width;

const Discover = ({item}: {item: ITarot}) => {
  const [data, setData] = React.useState<ITarot[]>([]);
  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();
  const [loading, setLoading] = React.useState<boolean>(false);

  React.useEffect(() => {
    fetch(URL.Cards)
      .then(response => response.json())
      .then(cards => {
        setData(cards);
        setLoading(false);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  // const goReading = React.useCallback(() => {
  //   navigate('ReadingDisc', {
  //     id: item.id,
  //     img: item.img,
  //     meaning_up: item.meaning_up,
  //     desc: item.desc,
  //   });
  // }, []);
  const onEndReached = React.useCallback(() => {
    setData((prev: ITarot[]) => prev.concat(data));
  }, []);

  const renderItem = React.useCallback(({item}: {item: ITarot}) => {
    return (
      <ItemRender
        backCard={item.backCard}
        id={item.id}
        name={item.name}
        value={item.value}
        img={item.img}
        meaning_up={''}
        meaning_rev={''}
        desc={''}
      />
    );
  }, []);

  return (
    <View style={{backgroundColor: 'grey'}}>
      <FlatList
        data={data}
        renderItem={renderItem}
        numColumns={2}
        scrollEnabled={true}
        removeClippedSubviews={false}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.5}
        onEndReached={onEndReached}
      />
    </View>
  );
};

export default Discover;

const styles = StyleSheet.create({});
