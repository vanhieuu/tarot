import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface ICardTarotState {
  id: string;
  name: string;
  desc: string;
  meaning_up: string;
  img: string;
}

const initValue: ICardTarotState = {
  name: '',
  id: '',
  desc: '',
  meaning_up: '',
  img: '',
};

export const tarotCardSlice = createSlice({
  name: 'tarotCard',
  initialState: initValue,
  reducers: {
    onUpdateCard: (state, action: PayloadAction<ICardTarotState>) => {
      (state.name = action.payload.name),
        (state.id = action.payload.id),
        (state.desc = action.payload.desc),
        (state.meaning_up = action.payload.meaning_up);
      state.img = action.payload.img;
    },
  },
});
export const {onUpdateCard} = tarotCardSlice.actions;

export default tarotCardSlice.reducer;
