import { configureStore } from "@reduxjs/toolkit";
import  tarotCard  from "./cardSlice";

const store = configureStore({
    reducer:{
        tarotCard
    }
})
export default store
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;