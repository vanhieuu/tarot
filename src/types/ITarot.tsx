import { ImageSourcePropType } from "react-native";

export interface ITarot {
  id: string;
  name: string;
  value: string;
  img: string
  backCard: string
  meaning_up: string;
  meaning_rev: string;
  desc: string;
}
export interface IResTarot{
  success:boolean,
  message:string,
  cards:ITarot[]
}